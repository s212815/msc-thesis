from pandas import DataFrame, Series, concat
from .sampler import Sampler, TruncatedMVN
from .daypattern import DayPatternSim, ACTIVITIES
from .prim import PrimClassifier
from typing import Tuple
from datetime import datetime
from tqdm import tqdm
import numpy as np
from enum import Enum
from sklearn.preprocessing import normalize
from scipy.stats import multivariate_normal

import torch
from botorch.models import SingleTaskGP
from botorch.models.transforms.input import Normalize
from botorch.models.transforms.outcome import Standardize
from gpytorch.mlls.exact_marginal_log_likelihood import ExactMarginalLogLikelihood
from botorch.fit import fit_gpytorch_mll

class Strategy(Enum):
    PRIM_BOX = 'PRIM_BOX'
    PRIM_PROBS_MEAN = 'PRIM_PROBS_MEAN'
    PRIM_PROBS_ALL = 'PRIM_PROBS_ALL'
    PRIM_PROBS_GP = 'PRIM_PROBS_GP'

class ActiveLearning():
    
    def __init__(self, strategy:Strategy, activity:str, threshold:float) -> None:
        self.strategy = strategy
        self.activity = activity
        self.threshold = threshold
        
    def _prim(self, cube:DataFrame, counts:DataFrame):
        """Run prim algorithm on the data
        
        Args:
            cube (DataFrame): model parameters sample
            counts (DataFrame): activity count

        Returns:
            PrimBox: box
        """
        activity, threshold = self.get_metadata()
        pc = PrimClassifier(
            activity = activity,
            threshold = threshold
        )
        box = pc.fit(cube, counts)
        return pc, box
    
    def get_metadata(self):
        activity = self.activity
        threshold = self.threshold
        return activity, threshold
    
    def get_next_points_prim_box(self, cube:DataFrame, counts:DataFrame, bounds:DataFrame, n_points:int=2):
        """Get the next point using the prim_box strategy

        Args:
            cube (DataFrame): model parameters sample
            counts (DataFrame): activity count
            n_points (int, optionnal): number of points to sample
        """
        _, box = self._prim(cube, counts)
        x_new = Sampler(box.limits).create_sample_lhs(n_points)
        return x_new
    
    def draw_sample(self, dist, cube:DataFrame, bounds:DataFrame, limits:DataFrame):
        if len(limits) > 0:
            sample = DataFrame(dist.sample(1), index=limits.index).transpose()
            X = Sampler(bounds).create_sample_lhs(1)
            X = X.drop(limits.index, axis=1)
            sample = concat([sample, X], axis=1)[cube.columns]
        else:
            sample = Sampler(bounds).create_sample_lhs(1)
        return sample
        
    def get_next_points_prim_probs_all(self, cube:DataFrame, counts:DataFrame, bounds:DataFrame) -> DataFrame:
        pc, box = self._prim(cube, counts)
        N = box.peeling_trajectory.shape[0]
        # print(box.peeling_trajectory)
        samples = DataFrame()
        for i in range(N):
            box.select(i)
            limits = box.limits 
            limits['center'] = (limits['min'] + limits['max'])/2
            
            bounds.index = bounds['name']
            lb = bounds[bounds['name'].isin(limits.index)]['min'][limits.index].values
            ub = bounds[bounds['name'].isin(limits.index)]['max'][limits.index].values
            
            mask = [True if c in limits.index else False for c in cube.columns]
            cov = cube.cov().iloc[mask, mask].values
            tmvn = TruncatedMVN(limits['center'].values, cov, lb, ub)
            sample = self.draw_sample(tmvn, cube, bounds, limits)
            samples = concat([samples, sample], axis=0)
        return samples
            
    
    def get_next_points_prim_probs_mean(self, cube:DataFrame, counts:DataFrame, bounds:DataFrame) -> DataFrame:
        """Get the next point using the prim_box strategy
           Next points is draw from a multivariate distribution centered on the box, the min boundary and the max boundary

        Args:
            cube (DataFrame): _description_
            counts (DataFrame): _description_
        """
        pc, box = self._prim(cube, counts)
        limits = box.limits 
        limits['center'] = (limits['min'] + limits['max'])/2
        
        bounds.index = bounds['name']
        lb = bounds[bounds['name'].isin(limits.index)]['min'][limits.index].values
        ub = bounds[bounds['name'].isin(limits.index)]['max'][limits.index].values
        
        mask = [True if c in limits.index else False for c in cube.columns]
        cov = cube.cov().iloc[mask, mask].values
        tmvn = TruncatedMVN(limits['center'].values, cov, lb, ub)
        sample = self.draw_sample(tmvn, cube, bounds, limits)
        
        return sample

    def _fit_gp(self, cube:DataFrame, y_probs:DataFrame) -> SingleTaskGP:
        X = torch.Tensor(cube.values).to(torch.float64)
        Y = torch.Tensor(y_probs.values).to(torch.float64).unsqueeze(1)

        gp = SingleTaskGP(X, Y, input_transform=Normalize(d=X.shape[-1]), outcome_transform=Standardize(m=Y.shape[-1]))
        mll = ExactMarginalLogLikelihood(gp.likelihood, gp)
        fit_gpytorch_mll(mll)
        return gp

    def get_next_points_prim_probs_gp(self, cube:DataFrame, counts:DataFrame, bounds:DataFrame):
        """Get the next point using the prim_box strategy
           Next point is the mean of the 10 points with the highest probability

        Args:
            cube (DataFrame): _description_
            counts (DataFrame): _description_
        """
        pc, box = self._prim(cube, counts)
        y_probs = pc._compute_proba(cube, counts)

        gp = self._fit_gp(cube, y_probs)
        X = torch.Tensor(Sampler(bounds, 10000).create_sample_lhs().values)
        y = gp.posterior(X).mean.detach().numpy()
        idx = y.argmax()
        x_new = DataFrame(X[idx].numpy(), index=cube.columns).transpose()
        
        return x_new
    
    def get_next_points(self, cube:DataFrame, counts:DataFrame, bounds:DataFrame=None, n_points:int=2):
        if self.strategy == 'PRIM_BOX':
            return self.get_next_points_prim_box(cube, counts, bounds, n_points)
        elif self.strategy == 'PRIM_PROBS_MEAN':
            return self.get_next_points_prim_probs_mean(cube, counts, bounds)
        elif self.strategy == 'PRIM_PROBS_ALL':
            return self.get_next_points_prim_probs_all(cube, counts, bounds)
        elif self.strategy == 'PRIM_PROBS_GP':
            return self.get_next_points_prim_probs_gp(cube, counts, bounds)
        else:
            raise ValueError("Strategy non implemented")
        
    def evaluate_next_points(self, DP:DayPatternSim, x_new:DataFrame) -> DataFrame:
        """_summary_

        Args:
            x_nex (DataFrame): _description_

        Returns:
            DataFrame: _description_
        """
        if isinstance(x_new, type(None)):
            raise ValueError
        else:
            y_new = DP.run(x_new)
        return y_new
    
    def sampling_metric_true_value(self, y_activity:Series, threshold:float):
        return (y_activity > threshold).sum()
        
    def update_data(self, cube:DataFrame, counts:DataFrame, y_new:DataFrame) -> Tuple[DataFrame, ...]:
        activity, _ = self.get_metadata()
        counts = concat([counts, y_new[activity]]).reset_index().drop('index', axis=1).squeeze()
        cube = concat([cube, y_new[cube.columns]]).reset_index().drop('index', axis=1)
        return cube, counts
    
    def _run_one_step(self, DP:DayPatternSim, cube:DataFrame, counts:DataFrame, bounds:DataFrame, n_points:int):
        x_new = self.get_next_points(cube, counts, bounds, n_points)
        y_new = self.evaluate_next_points(DP, x_new)
        cube, counts = self.update_data(cube, counts, y_new)
        
        if self.strategy == 'PRIM_BOX':
            x_new = Sampler(bounds).create_sample_lhs(10000).sample(1)
            y_new = self.evaluate_next_points(DP, x_new)
            cube, counts = self.update_data(cube, counts, y_new)
        
        # Get the number of sample that gives a count > threshold
        activity, threshold = self.get_metadata()
        f = self.sampling_metric_true_value(y_new[activity], threshold)
        return cube, counts
    
    def run_active_sampling(self, DP:DayPatternSim, cube:DataFrame, counts:DataFrame, n_iter:int, bounds:DataFrame=None, save:bool=False, n_points:int=2):
        for _ in tqdm(range(n_iter)):
            cube, counts = self._run_one_step(DP, cube, counts, bounds, n_points)
        if save:
            data = cube.merge(counts, left_index=True, right_index=True)
            timestamp = datetime.now().timestamp()
            data.to_csv(f'data/out/{timestamp}.csv')
        return cube, counts

        
    