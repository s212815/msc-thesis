## Define model specific utility functions

import numpy as np
import time
from datetime import datetime
from pandas import DataFrame, Series, read_csv, concat, merge
from sklearn.base import BaseEstimator
from tqdm import tqdm
from utils.decorators import background, make_parallel

def utility_dpb(betas:dict, df:DataFrame, binaries:DataFrame,i:int) -> Series:    
    return (betas['tour_edu'] * binaries['EduT'].iloc[i] + betas['tour_personal'] * binaries['PersonalT'].iloc[i] +\
    betas['tour_leisure'] * binaries['LeisureT'].iloc[i] + betas['tour_shop'] * binaries['ShopT'].iloc[i] + betas['tour_escort'] * binaries['EscortT'].iloc[i]+\
    betas['twotours'] * binaries['twotour'].iloc[i] +\
    betas['fourtours'] * binaries['fourtour'].iloc[i] +\
    betas['workpersonal_tt'] * binaries['workpersonal_tt'].iloc[i] + betas['workleisure_tt'] * binaries['workleisure_tt'].iloc[i] +\
    betas['workshop_tt'] * binaries['workshop_tt'].iloc[i] + betas['workescort_tt'] * binaries['workescort_tt'].iloc[i] +\
    betas['edupersonal_tt'] * binaries['edupersonal_tt'].iloc[i] + betas['edushop_tt'] * binaries['edushop_tt'].iloc[i] + betas['eduleisure_tt'] * binaries['eduleisure_tt'].iloc[i] +\
    betas['eduescort_tt'] * binaries['eduescort_tt'].iloc[i] +\
    betas['personalshop_tt'] * binaries['personalshop_tt'].iloc[i] + betas['personalleisure_tt'] * binaries['personalleisure_tt'].iloc[i] + betas['personalescort_tt'] * binaries['personalescort_tt'].iloc[i] +\
    betas['shopleisure_tt'] * binaries['shopleisure_tt'].iloc[i] + betas['shopescort_tt'] * binaries['shopescort_tt'].iloc[i] +\
    betas['leisureescort_tt'] * binaries['leisureescort_tt'].iloc[i] +\
    betas['parttime_edu'] * binaries['EduT'].iloc[i] * df['parttime'] + betas['parttime_personal'] * binaries['PersonalT'].iloc[i] * df['parttime'] +\
    betas['parttime_leisure'] * binaries['LeisureT'].iloc[i] * df['parttime'] + betas['parttime_shop'] * binaries['ShopT'].iloc[i] * df['parttime'] + betas['parttime_escort'] * binaries['EscortT'].iloc[i] * df['parttime'] +\
    betas['retired_edu'] * binaries['EduT'].iloc[i] * df['retired'] + betas['retired_personal'] * binaries['PersonalT'].iloc[i] * df['retired'] +\
    betas['retired_leisure'] * binaries['LeisureT'].iloc[i] * df['retired'] + betas['retired_shop'] * binaries['ShopT'].iloc[i] * df['retired'] + betas['retired_escort'] * binaries['EscortT'].iloc[i] * df['retired']  +\
    betas['disabled_edu'] * binaries['EduT'].iloc[i] * df['disabled'] + betas['disabled_personal'] * binaries['PersonalT'].iloc[i] * df['disabled'] +\
    betas['disabled_leisure'] * binaries['LeisureT'].iloc[i] * df['disabled'] + betas['disabled_shop'] * binaries['ShopT'].iloc[i] * df['disabled'] + betas['disabled_escort'] * binaries['EscortT'].iloc[i] * df['disabled']  +\
    betas['homemaker_edu'] * binaries['EduT'].iloc[i] * df['homemaker'] + betas['homemaker_personal'] * binaries['PersonalT'].iloc[i] * df['homemaker'] +\
    betas['homemaker_leisure'] * binaries['LeisureT'].iloc[i] * df['homemaker'] + betas['homemaker_shop'] * binaries['ShopT'].iloc[i] * df['homemaker'] + betas['homemaker_escort'] * binaries['EscortT'].iloc[i] * df['homemaker']  +\
    betas['OnLeave_edu'] * binaries['EduT'].iloc[i] * df['onLeave'] + betas['OnLeave_personal'] * binaries['PersonalT'].iloc[i] * df['onLeave'] +\
    betas['OnLeave_leisure'] * binaries['LeisureT'].iloc[i] * df['onLeave'] + betas['OnLeave_shop'] * binaries['ShopT'].iloc[i] * df['onLeave'] + betas['OnLeave_escort'] * binaries['EscortT'].iloc[i] * df['onLeave']  +\
    betas['unemployed_edu'] * binaries['EduT'].iloc[i] * df['unemployed'] + betas['unemployed_personal'] * binaries['PersonalT'].iloc[i] * df['unemployed'] +\
    betas['unemployed_leisure'] * binaries['LeisureT'].iloc[i] * df['unemployed'] + betas['unemployed_shop'] * binaries['ShopT'].iloc[i] * df['unemployed'] + betas['unemployed_escort'] * binaries['EscortT'].iloc[i] * df['unemployed']  +\
    betas['universitystudent_edu'] * binaries['EduT'].iloc[i] * df['universityStudent'] +\
    betas['universitystudent_personal'] * binaries['PersonalT'].iloc[i] * df['universityStudent'] + betas['universitystudent_leisure'] * binaries['LeisureT'].iloc[i] * df['universityStudent'] +\
    betas['universitystudent_shop'] * binaries['ShopT'].iloc[i] * df['universityStudent'] + betas['universitystudent_escort'] * binaries['EscortT'].iloc[i] * df['universityStudent']  +\
    betas['student_edu'] * binaries['EduT'].iloc[i] * df['student'] + betas['student_personal'] * binaries['PersonalT'].iloc[i] * df['student'] +\
    betas['student_leisure'] * binaries['LeisureT'].iloc[i] * df['student'] + betas['student_shop'] * binaries['ShopT'].iloc[i] * df['student'] + betas['student_escort'] * binaries['EscortT'].iloc[i] * df['student']  +\
    betas['OtherStudent_edu'] * binaries['EduT'].iloc[i] * df['other_student'] + betas['OtherStudent_personal'] * binaries['PersonalT'].iloc[i] * df['other_student'] +\
    betas['OtherStudent_leisure'] * binaries['LeisureT'].iloc[i] * df['other_student'] + betas['OtherStudent_shop'] * binaries['ShopT'].iloc[i] * df['other_student'] + betas['OtherStudent_escort'] * binaries['EscortT'].iloc[i] * df['other_student']  +\
    betas['Trainee_edu'] * binaries['EduT'].iloc[i] * df['trainee'] + betas['Trainee_personal'] * binaries['PersonalT'].iloc[i] * df['trainee'] +\
    betas['Trainee_leisure'] * binaries['LeisureT'].iloc[i] * df['trainee'] + betas['Trainee_shop'] * binaries['ShopT'].iloc[i] * df['trainee'] + betas['Trainee_escort'] * binaries['EscortT'].iloc[i] * df['trainee']  +\
    betas['ageUpto19_edu'] * binaries['EduT'].iloc[i] * df['ageUpto19'] + betas['ageUpto19_personal'] * binaries['PersonalT'].iloc[i] * df['ageUpto19'] +\
    betas['ageUpto19_leisure'] * binaries['LeisureT'].iloc[i] * df['ageUpto19'] + betas['ageUpto19_shop'] * binaries['ShopT'].iloc[i] * df['ageUpto19'] + betas['ageUpto19_escort'] * binaries['EscortT'].iloc[i] * df['ageUpto19'] +\
    betas['age2025_edu'] * binaries['EduT'].iloc[i] * df['age2025'] + betas['age2025_personal'] * binaries['PersonalT'].iloc[i] * df['age2025'] +\
    betas['age2025_leisure'] * binaries['LeisureT'].iloc[i] * df['age2025'] + betas['age2025_shop'] * binaries['ShopT'].iloc[i] * df['age2025'] + betas['age2025_escort'] * binaries['EscortT'].iloc[i] * df['age2025']  +\
    betas['age2635_edu'] * binaries['EduT'].iloc[i] * df['age2635'] + betas['age2635_personal'] * binaries['PersonalT'].iloc[i] * df['age2635'] +\
    betas['age2635_leisure'] * binaries['LeisureT'].iloc[i] * df['age2635'] + betas['age2635_shop'] * binaries['ShopT'].iloc[i] * df['age2635'] + betas['age2635_escort'] * binaries['EscortT'].iloc[i] * df['age2635']  +\
    betas['age5165_edu'] * binaries['EduT'].iloc[i] * df['age5165'] + betas['age5165_personal'] * binaries['PersonalT'].iloc[i] * df['age5165'] +\
    betas['age5165_leisure'] * binaries['LeisureT'].iloc[i] * df['age5165'] + betas['age5165_shop'] * binaries['ShopT'].iloc[i] * df['age5165'] + betas['age5165_escort'] * binaries['EscortT'].iloc[i] * df['age5165']  +\
    betas['age65_edu'] * binaries['EduT'].iloc[i] * df['ageMorethan65'] + betas['age65_personal'] * binaries['PersonalT'].iloc[i] * df['ageMorethan65'] +\
    betas['age65_shop'] * binaries['ShopT'].iloc[i] * df['ageMorethan65'] + betas['age65_escort'] * binaries['EscortT'].iloc[i] * df['ageMorethan65']  +\
    betas['female_edu'] * binaries['EduT'].iloc[i] * df['female'] + betas['female_personal'] * binaries['PersonalT'].iloc[i] * df['female'] +\
    betas['female_leisure'] * binaries['LeisureT'].iloc[i] * df['female'] + betas['female_shop'] * binaries['ShopT'].iloc[i] * df['female'] + betas['female_escort'] * binaries['EscortT'].iloc[i] * df['female']  +\
    betas['fam_income_edu'] * binaries['EduT'].iloc[i] * df['IncomeScaled'] + betas['fam_income_personal'] * binaries['PersonalT'].iloc[i] * df['IncomeScaled'] +\
    betas['fam_income_leisure'] * binaries['LeisureT'].iloc[i] * df['IncomeScaled'] + betas['fam_income_shop'] * binaries['ShopT'].iloc[i] * df['IncomeScaled'] + betas['fam_income_escort'] * binaries['EscortT'].iloc[i] * df['IncomeScaled']  +\
    betas['LIC_edu'] * binaries['EduT'].iloc[i] * df['DRVLC'] + betas['LIC_personal'] * binaries['PersonalT'].iloc[i] * df['DRVLC'] + betas['LIC_rec'] * binaries['LeisureT'].iloc[i] * df['DRVLC'] +\
    betas['LIC_shop'] * binaries['ShopT'].iloc[i] * df['DRVLC'] + betas['LIC_escort'] * binaries['EscortT'].iloc[i] * df['DRVLC']  +\
    betas['TRANS_edu'] * binaries['EduT'].iloc[i] * df['PTPASS'] + betas['TRANS_personal'] * binaries['PersonalT'].iloc[i] * df['PTPASS'] +\
    betas['TRANS_rec'] * binaries['LeisureT'].iloc[i] * df['PTPASS'] + betas['TRANS_shop'] * binaries['ShopT'].iloc[i] * df['PTPASS'] + betas['TRANS_escort'] * binaries['EscortT'].iloc[i] * df['PTPASS'])
    
    
def utility_dp(betas:dict, df:DataFrame, binaries:DataFrame, i:int) -> Series:
    if i == 1:
        return Series(np.zeros(df.shape[0]))
    elif i == 2:
        return(betas['cons_travel'] +\
            betas['parttime_travel'] * df['parttime'] +\
            betas['retired_travel'] * df['retired'] +\
            betas['disabled_travel'] * df['disabled'] +\
            betas['homemaker_travel'] * df['homemaker'] +\
            betas['OnLeave_travel'] * df['onLeave'] +\
            betas['unemployed_travel'] * df['unemployed'] +\
            betas['universitystudent_travel'] * df['universityStudent'] +\
            betas['student_travel'] * df['student'] +\
            betas['OtherStudent_travel'] * df['other_student'] +\
            betas['ageUpto19_travel'] * df['ageUpto19'] +\
            betas['age2025_travel'] * df['age2025'] +\
            betas['age2635_travel'] * df['age2635'] +\
            betas['age5165_travel'] * df['age5165'] +\
            betas['age65_travel'] * df['ageMorethan65'] +\
            betas['female_travel'] * df['female'] +\
            betas['fam_income_travel'] * df['FAM_INC_imputed'] +\
            betas['LIC_travel'] * df['DRVLC'] +\
            betas['TRANS_travel'] * df['PTPASS'])



UTILITY_FUNCTION = {
    "dp": utility_dp,
    "dpb": utility_dpb
}

binaries = read_csv('models/meta/binaries.csv')
BINARIES = {
    "dpb": binaries,
    "dp": None
}

ACTIVITIES = ["WorkT", "EduT", "PersonalT", "ShopT", "LeisureT", "EscortT"]

class DayPattern(BaseEstimator):
    
    def __init__(self, betas:dict) -> None:
        super().__init__()
        self.betas = betas
        self.binaries = BINARIES
        self.utility_function = UTILITY_FUNCTION
        
    def _compute_utility(self, X, model:str) -> np.ndarray:
        """
        Args:
            X (DataFrame): _description_
            uf (int): index of the utility function

        Returns:
            np.ndarray: utility vector
        """
        V = []
        try:
            M = self.binaries[model].shape[0]-2
        except:
            M = 2
        for i in range(1, M+1):
            V.append(self.utility_function[model](self.betas, X, self.binaries[model], i))
        V = np.array(V).astype(float)
        return V
    
    def _compute_probability(self, V:np.ndarray) -> np.ndarray:
        """
        Args:
            V (np.ndarray): Utility vector

        Returns:
            np.ndarray: probability vector
        """
        M = V.shape[0]
        d = np.exp(V).sum(axis=0)
        p = np.array([np.exp(V[i,:])/d for i in range(M)])
        return p
    
    def _compute_choices(self,p:np.ndarray) -> np.ndarray:
        """
        Args:
            p (np.ndarray): probability vector

        Returns:
            np.ndarray: choice vector
        """
        M,N = p.shape
        try:
            choices = np.array([np.random.choice(np.arange(0,M), p = p[:,i]) for i in range(N)])
            return choices
        except:
            return None
        
    
    def _compute_activities_count(self, choices: np.ndarray) -> Series:
        """
        Args: 
            choices (np.ndarray): choices vector
        Returns:
            np.ndarray: activity count vector
        """
        N = choices.shape[0]
        act_counts = np.empty(len(ACTIVITIES))
        for k,activity in enumerate(ACTIVITIES):
            act = np.array([1 if self.binaries['dpb'][activity][choices[i]] == 1 else 0 for i in range(N)])
            act_counts[k] = act.sum()
        act_counts = Series(act_counts, index=ACTIVITIES)
        return act_counts
        
    def fit(self, X, y=None):
        """_summary_

        Args:
            X (_type_): _description_
            y : Ignored
        """
        # Compute single model individually
        
        V_dpb = self._compute_utility(X, 'dpb')
        p_dpb = self._compute_probability(V_dpb)
        
        V_dp = self._compute_utility(X, 'dp')
        p_dp = self._compute_probability(V_dp)       
        
        # Compute the model combination probability
        N = p_dp.shape[1]
        p_stay= p_dp[0].reshape((1, N))
        p_new = p_dp[1] * p_dpb
        p_new = np.concatenate((p_stay, p_new), axis= 0)

        choices = self._compute_choices(p_new)
        
        return choices
  
        
    def fit_predict(self, X, y=None):
        """
        Predict the model evaluation based on a given scoring function
        Args
        ----------
        X : {array-like, sparse matrix} of shape (n_samples, n_features)
            New data to evaluate
        y : Ignored
        Returns
        -------
        activity counts
        """
        choices = self.fit(X,y)
        y = self._compute_activities_count(choices)
        return Series(y)
    
    
    
class DayPatternSim(DayPattern):
    """Extend the DayPattern model to simulate multiple samples
    """
    
    def __init__(self, BETAS:dict, df:DataFrame) -> None:
        self.df = df 
        self.BETAS = BETAS 
        
    def _run_sample(self, cube:DataFrame, idx:int) -> Series:
        """
        Args:
            df (DataFrame): inputs of the dp model
            idx (int): row of the cube to sample

        Returns:
            Series: one iteration of the DP model
        """
        sample = dict(cube.iloc[idx])
        for key, value in self.BETAS.items():
            if key not in sample:
                sample[key] = value
        st = time.time()
        y = DayPattern(sample).fit_predict(self.df)
        et = time.time()
        y = concat([DataFrame(sample, index=[0]).transpose(), y, Series({'execution_time': et-st})])
        return y
        
    # @background
    def run(self, cube:DataFrame, save:bool=False) -> DataFrame:
        """Run the DayPattern module on the cube
        
        Args:
            cube (DataFrame): parameters of the dp model
            
        Returns:
            DataFrame: Cube and Counts of activities for each sample of the cube
        """
        res = DataFrame()
        for idx in range(cube.shape[0]):
            y = self._run_sample(cube, idx)
            res = concat([res, y], axis=1)
        if save:
            timestamp = datetime.now().timestamp()
            res.transpose().to_csv(f'data/out/{timestamp}.csv')
        return res.transpose()
        