from prim import Prim
from typing import Tuple, Any
from pandas import DataFrame, Series, read_csv
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt 
from sklearn.metrics import roc_curve, RocCurveDisplay, auc
from sklearn.base import BaseEstimator

def score(box):
    # Compute harmonic mean to define precision of boxes
    traj = box.peeling_trajectory
    c, d, i = traj['coverage'], traj['density'], traj['res dim']
    traj['harmonic mean'] = 3 * c * d * i / (c*d + d*i + c*i)
    f = traj['harmonic mean'].max()
    return traj, f

def select_optimal_box(box, mode:str='harmonic mean'):
    # Select box with highest harmonic mean
    traj, _ = score(box)
    traj['harmonic mean'] = traj['harmonic mean'].astype(float)
    idx = traj[mode].idxmax()
    box.select(idx)
    return box

def coverage(y_bin:Series, y_pred:Series) -> float:
    mask_true = np.array(y_bin == 1).astype(int)
    mask_pred = np.array(y_pred == 1).astype(int)
    true_positive = Series(mask_true + mask_pred).value_counts()[2]
    true_negative = Series(mask_true).value_counts()[1]
    return true_positive / true_negative

def density(y_bin:Series, y_pred:Series) -> float:
    mask_true = np.array(y_bin == 1).astype(int)
    mask_pred = np.array(y_pred == 1).astype(int)
    true_positive = Series(mask_true + mask_pred).value_counts()[2]
    false_positive = Series(mask_pred).value_counts()[1]
    return true_positive / false_positive


params = read_csv('data/params.csv', index_col='Unnamed: 0')

class PrimClassifier(BaseEstimator):
    def __init__(self, activity:str, threshold:float, peel_alpha:float=0.05, paste_alpha:float=0.05):
        self.activity = activity
        self.threshold = threshold
        self.peel_alpha = peel_alpha
        self.paste_alpha = paste_alpha
        
    def set_threshold(self, threshold):
        self.threshold = threshold
        
    def get_box(self):
        print(self.box)
    
    def get_prim(self):
        return self._prim
    
    def set_box(self, idx:int):
        self.box.select(idx)
    
    def predict(self, X:DataFrame, limits:DataFrame=None) -> Series:
        mask = Series([True] * X.shape[0])
        if isinstance(limits, type(None)):
            for k, idx in enumerate(self.box.limits.index):
                s = X[idx]
                a = self.box.limits['min'][k]
                b = self.box.limits['max'][k]
                mask = ((s >= a) & (s <= b)) & mask
            y_pred = mask.astype(int)
        else:
            for k, idx in enumerate(limits.index):
                s = X[idx]
                a = limits['min'][k]
                b = limits['max'][k]
                mask = ((s >= a) & (s <= b)) & mask
            y_pred = mask.astype(int)
        return y_pred
    
    def fit(self, X:DataFrame, y:DataFrame):
        y_bin = self._compute_bin(y)
        box = self._compute_box(X,y_bin)
        self.box = box
        return box
    
    def _compute_bin(self, y:DataFrame) -> Series:
        return y > self.threshold
        
    def _compute_box(self, X:DataFrame, y:Series):
         #params['threshold'][activity]
        p = Prim(X,y, 0.5, ">", peel_alpha=self.peel_alpha, paste_alpha=self.paste_alpha)
        box = p.find_box()
        box = select_optimal_box(box)
        self._prim = p
        return box 
    
    def _compute_proba2(self, X:DataFrame, y:Series):
        N = self.box.peeling_trajectory.shape[0]
        y_proba = np.empty((N, len(X)))
        traj, _= score(self.box)
        W = np.empty((N,1))
        print(traj)
        for idx in range(N):
            self.box.select(idx)
            y_pred = self.predict(X)
            y_proba[idx] = y_pred
        y_probs = y_proba.sum(axis=0)
        print(Series(y_probs.squeeze()))
        return Series(y_probs.squeeze())
    
    def _compute_roc(self, X:DataFrame, y:DataFrame) -> Tuple[Any, ...]:
        y_bin = self._compute_bin(y)
        y_pred = self.predict(X)
        fpr, tpr, _= roc_curve(y_bin, y_pred)
        fpr, tpr = fpr[1:-1], tpr[1:-1]
        return fpr, tpr
    
    def compute_roc_curve(self, X:DataFrame, y:DataFrame, n_points:int=10):
        mu = params['mu'][self.activity] 
        std = params['std'][self.activity]
        THRESHOLD = np.linspace(mu - 1.96 * std, mu +  1.96 * std, n_points)
        FPR, TPR = np.zeros(n_points+2), np.zeros(n_points+2)
        FPR[0], TPR[0] = 0, 0
        FPR[-1], TPR[-1] = 1, 1
        for k,t in enumerate(THRESHOLD):
            self.set_threshold(t)
            FPR[k+1], TPR[k+1] = self._compute_roc(X, y)
        TPR = [x for x, _ in sorted(zip(TPR, FPR))]
        FPR = [x for x, _ in sorted(zip(FPR, TPR))]
        return FPR, TPR
    
    def display_roc_curve(self, FPR, TPR):
        roc_auc = auc(FPR, TPR)
        RocCurveDisplay(
            fpr=FPR,
            tpr=TPR,
            roc_auc=roc_auc,
            estimator_name='PRIM Classifier'
        ).plot(marker='.', color='#990000')
        plt.plot([0, 1], [0, 1], "k--", label="Baseline (AUC = 0.5)")
        plt.legend()
        # plt.title(self.activity)
        plt.show()
        
    def score(self, X, y) -> float:
        box = self.fit(X,y)
        return box.density
    
    def get_limits(self, cube_scaled:DataFrame) -> DataFrame:
        limits = self.box.limits
        limits['parameter'] = limits.index
        limits['min_norm'] = (limits['min'] - cube_scaled[limits['parameter']].min(axis=0)) / (cube_scaled[limits['parameter']].max(axis=0) - cube_scaled[limits['parameter']].min(axis=0))
        limits['max_norm'] = (limits['max'] - cube_scaled[limits['parameter']].min(axis=0)) / (cube_scaled[limits['parameter']].max(axis=0) - cube_scaled[limits['parameter']].min(axis=0))
        return limits 
    
    def get_partial_metrics(self, cube:DataFrame, counts:DataFrame, idx:int) -> Tuple[float, ...]:
        box = self.fit(cube,counts)
        limits = box.limits
        y_pred = self.predict(cube, limits.iloc[:idx])
        y_bin = self._compute_bin(counts)
        cov = coverage(y_bin, y_pred)
        den = density(y_bin, y_pred)
        return cov, den
        
    
    def plot_scenario_identification(self, cube_scaled:DataFrame, true_min_values:np.ndarray, true_max_values:np.ndarray, colors:list=None):
        limits = self.get_limits(cube_scaled)
        sns.barplot(data=limits, x='max_norm', y='parameter', palette=sns.color_palette(colors))
        sns.barplot(data=limits, x='min_norm', y='parameter', color='white')
        for i in range(len(limits)):
            plt.text(limits['min_norm'].iloc[i], i, f"{true_min_values[i]:.2f}", va='center', ha='left', color='white')
            plt.text(limits['max_norm'].iloc[i], i, f"{true_max_values[i]:.2f}", va='center', ha='right', color='white')
        plt.xlabel('Normalized dimension')

