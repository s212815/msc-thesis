import pandas as pd
from datetime import datetime

# Specific imports
from utils.data_loader import load_metadata, load_data
from utils.decorators import benchmark
from models.sampler import Sampler
from models.daypattern import ACTIVITIES, DayPatternSim
from models.active_learning import ActiveLearning, Strategy

ACTIVITY = 'LeisureT'
THRESHOLD = 7900
SIZE = 5

def main():
    # Generate LHS sampling 
    DF, BETAS, BOUNDS = load_metadata()
    # cube = Sampler(BOUNDS).create_sample_lhs(SIZE)
    
    
    # DP = DayPatternSim(BETAS, DF)
    # print(DP.run(cube))
    
    
    # PRIM BOX STRATEGY
    AL = ActiveLearning('PRIM_BOX', ACTIVITY, THRESHOLD)
    DP = DayPatternSim(BETAS, DF)

    from wakepy import keep

    with keep.running() as k:
        for size in [300, 400, 500, 600, 700, 800, 900]:
            data = load_data('1683818388.570091')
            cube, counts = data.drop(ACTIVITIES + ['execution_time'], axis=1), data[ACTIVITY]
            cube, counts = cube.iloc[:size], counts.iloc[:size]
            cube, counts = AL.run_active_sampling(DP, cube, counts, 400, BOUNDS)
            df = pd.concat([cube, counts], axis=1)
            timestamp = datetime.now().timestamp()
            print(f"Run saved under data/out/PB/{timestamp}-{size}.csv")
            df.to_csv(f'data/out/PB/{timestamp}-{size}.csv')
        

if __name__ == "__main__":
    main()