def hex_to_rgb(value):
    '''
    Converts hex to rgb colours
    value: string of 6 characters representing a hex colour.
    Returns: list length 3 of RGB values'''
    value = value.strip("#") # removes hash symbol if present
    lv = len(value)
    return tuple(int(value[i:i + lv // 3], 16) for i in range(0, lv, lv // 3))


def rgb_to_dec(value):
    '''
    Converts rgb to decimal colours (i.e. divides each value by 256)
    value: list (length 3) of RGB values
    Returns: list (length 3) of decimal values'''
    return [v/256 for v in value]

DTU_COLORS_RGB = {
    'red': (153, 0, 0),
    'blue': (47, 62, 234),
    'green': (31, 208, 130),
    'navy_blue': (3, 15, 79),
    'yellow': (246, 208, 77),
    'orange': (252, 118, 52)
}

DTU_COLORS_DEC = {
    'red': rgb_to_dec((153, 0, 0)),
    'blue': rgb_to_dec((47, 62, 234)),
    'green': rgb_to_dec((31, 208, 130)),
    'navy_blue': rgb_to_dec((3, 15, 79)),
    'yellow': rgb_to_dec((246, 208, 77)),
    'orange': rgb_to_dec((252, 118, 52))
}

DTU_COLOR_HEX = {
    'red': "#990000",
    'blue': "#2F3EEA",
    'green': "#1FD082",
    'navy_blue': "#030F4F",
    'yellow': "#F6D04D",
    'orange': "#FC7634"
}


DTU_BIN = [DTU_COLORS_DEC.get('navy_blue'), DTU_COLORS_DEC.get('red')]
DTU_BIN_HC = [DTU_COLORS_DEC.get('blue'), DTU_COLORS_DEC.get('yellow')]

