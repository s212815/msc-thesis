import pandas as pd
from pandas import DataFrame
import json
import numpy as np
from typing import List

def load_betas(model:str):
    betas_dp = json.load(open("models/meta/betasDP.json"))
    betas_dpb = json.load(open("models/meta/betasDPB.json"))
    if model == 'dp':
        return betas_dp
    elif model == 'dpb':
        return betas_dpb
    else:
        raise ValueError(f"{model} is not a valid component.")


def load_metadata(names:List[str]=None):
    # Load inputs
    df = pd.read_csv("data/Data_1dpb_2dpt_6purp.csv")
    df['student'] = df['preschool_student'] + df['primaryschool_student'] + df['highschool_student']
    df['IncomeScaled'] = df['FAM_INC_imputed'] / 1000
    
    ##Load model parameters
    betas_dp = json.load(open("models/meta/betasDP.json"))
    betas_dp_sig = json.load(open("models/meta/betasDP_sig.json"))
    betas_dpb = json.load(open("models/meta/betasDPB.json"))
    betas = {**betas_dpb,**betas_dp} 
    
    # Create parameter range
    bounds = pd.DataFrame(columns=['name', 'min', 'max'])
    bounds['name'] = list(betas_dp.keys())
    mu, sig = np.array(list(betas_dp.values())), np.array(list(betas_dp_sig.values()))
    bounds['min'], bounds['max'], bounds['mu'] = mu - 1.96*sig, mu + 1.96*sig, mu
    bounds = bounds[bounds['name'] != 'cons_travel']
    bounds['min'] = round(bounds['min'].astype(float),5)
    bounds['max'] = round(bounds['max'].astype(float),5)
    
    # Filter on the desired features
    if names == None:
        names = list(betas_dp.keys())[:-1]
    bounds = bounds[bounds['name'].isin(names)]
        
    return df, betas, bounds

def load_data(filename:str, folder:str='LHS') -> DataFrame:
    """_summary_

    Args:
        filename (str): csv file
    Returns:
        DataFrame: experimental data
    """
    data = pd.read_csv(f'data/out/{folder}/{filename}.csv')
    data = data.drop(['Unnamed: 0', 'index'], axis=1)
    return data
    