### Create BO sampling using AX plaform

# Non Ax/BOTorch packages
import numpy as np
import matplotlib.pyplot as plt
import torch
import pandas as pd
import plotly.io as pio
from datetime import datetime
from typing import List, Type
from tqdm import tqdm
pio.renderers.default = "png"

# Scneario discovery 
from models.daypattern import DayPatternSim, ACTIVITIES
from models.prim import PrimClassifier, score, coverage, density
from models.sampler import Sampler, TruncatedMVN
from utils.data_loader import load_metadata, load_betas

# Ax imports
from ax.plot.trace import optimization_trace_single_method
from ax.plot.slice import interact_slice
from ax.core import arm, observation
from ax.modelbridge.torch import TorchModelBridge
from ax.modelbridge.registry import Models
from ax.service.utils.report_utils import exp_to_df
# from ax.utils.notebook.plotting import render, init_notebook_plotting

from ax import (
    ParameterType, 
    RangeParameter,
    SearchSpace, 
    Experiment, 
    OptimizationConfig,
    Objective,
    Metric,
    Data,
    Runner,
)

# Ax wrappers for BoTorch components
from ax.service.ax_client import AxClient, ObjectiveProperties
from ax.models.torch.botorch_modular.model import BoTorchModel
from ax.models.torch.botorch_modular.surrogate import Surrogate
from ax.models.torch.botorch_modular.acquisition import Acquisition

# BoTorch components
from botorch.models.model import Model
from botorch.models import FixedNoiseGP, SingleTaskGP
#from botorch.models.gp_regression import FixedNoiseGP
from botorch.acquisition.monte_carlo import qExpectedImprovement, qNoisyExpectedImprovement, qUpperConfidenceBound
from botorch.acquisition.analytic import UpperConfidenceBound
from botorch.acquisition import ExpectedImprovement
from botorch.acquisition.active_learning import qNegIntegratedPosteriorVariance
from botorch.acquisition.knowledge_gradient import qKnowledgeGradient
from gpytorch.mlls.exact_marginal_log_likelihood import ExactMarginalLogLikelihood
from ax.modelbridge.modelbridge_utils import get_pending_observation_features
from ax.modelbridge.generation_strategy import GenerationStep, GenerationStrategy
from ax.modelbridge.registry import Models
from botorch.sampling.qmc import NormalQMCEngine

# Required for plotting in Plotly
# init_notebook_plotting(offline = False)
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
dtype = torch.float64


ACTIVITY = 'LeisureT'
THRESHOLD = 7900
filename = '1683818388.570091'

betas_dp = load_betas('dp')
names = list(betas_dp.keys())[:-1]

df, betas, bounds = load_metadata()

def create_cube_count(filename):
    data = pd.read_csv(f'data/out/LHS/{filename}.csv')
    data = data.drop(['Unnamed: 0'], axis=1)
    data_standardized = (data - data.mean()) / data.std()
    cube = data[names]
    counts = data[ACTIVITY]
    return cube, counts

DP = DayPatternSim(betas, df)

def f(x):
    return DP.run(x)['LeisureT']


gs = GenerationStrategy(
    steps=[
        # Quasi-random initialization step
        # GenerationStep(
        #     model=Models.SOBOL,
        #     num_trials=1,  # How many trials should be produced from this generation step
        # ),
        # Bayesian optimization step using the custom acquisition function
        GenerationStep(
            model=Models.BOTORCH_MODULAR,
            num_trials=-1,  # No limitation on how many trials should be produced from this step
            # For `BOTORCH_MODULAR`, we pass in kwargs to specify what surrogate or acquisition function to use.
            model_kwargs={
                "surrogate": Surrogate(SingleTaskGP, mll_class=ExactMarginalLogLikelihood),
                "botorch_acqf_class": ExpectedImprovement, 
                # "acquisition_options": {"beta": 100},
                # "acquisition_options":{"mc_points": NormalQMCEngine(1).draw(16),
                # "optimizer_options": {"sequential": False},
                },
        ),
    ]
)

parameters = [{"name": bounds.iloc[i,0], "type": "range", "bounds": [bounds.iloc[i,1], bounds.iloc[i,2]]} for i in range(len(bounds))]

# Initialize the client - AxClient offers a convenient API to control the experiment
# ax_client = AxClient(generation_strategy=gs)

def create_ax_client(verbose:bool):
    ax_client = AxClient(generation_strategy=gs, verbose_logging=verbose)
    ax_client.create_experiment(
        name="mobility_simulator_experiment",
        parameters=[{'name': 'LIC_travel', 'type': 'range', 'bounds': [0.67436, 0.88564]},
    {'name': 'OnLeave_travel', 'type': 'range', 'bounds': [-1.68884, -0.79116]},
    {'name': 'OtherStudent_travel','type': 'range','bounds': [-0.39134, 0.27114]},
    {'name': 'TRANS_travel', 'type': 'range', 'bounds': [0.30162, 0.48038]},
    {'name': 'age2025_travel', 'type': 'range', 'bounds': [-0.5092, -0.0388]},
    {'name': 'age2635_travel', 'type': 'range', 'bounds': [-0.1608, 0.2112]},
    {'name': 'age5165_travel', 'type': 'range', 'bounds': [-0.38331, -0.08069]},
    {'name': 'age65_travel', 'type': 'range', 'bounds': [-0.83756, -0.30444]},
    {'name': 'ageUpto19_travel', 'type': 'range', 'bounds': [-0.50744, 0.13544]},
    {'name': 'disabled_travel', 'type': 'range', 'bounds': [-1.69208, -1.30792]},
    {'name': 'fam_income_travel', 'type': 'range', 'bounds': [-4e-05, 0.00033]},
    {'name': 'female_travel', 'type': 'range', 'bounds': [0.0155, 0.1825]},
    {'name': 'homemaker_travel', 'type': 'range', 'bounds': [-2.00636, -1.45364]},
    {'name': 'parttime_travel', 'type': 'range', 'bounds': [-0.33297, 0.02297]},
    {'name': 'retired_travel', 'type': 'range', 'bounds': [-1.23692, -0.73908]},
    {'name': 'student_travel', 'type': 'range', 'bounds': [0.27876, 0.94124]},
    {'name': 'unemployed_travel', 'type': 'range', 'bounds': [-1.5348, -1.0252]},
    {'name': 'universitystudent_travel','type': 'range','bounds': [-0.7006, -0.1714]}],
        
        objectives={
            ACTIVITY: ObjectiveProperties(minimize=False),
            # "gp_std": ObjectiveProperties(minimize=True)
        }
    )
    return ax_client


def create_mutation(x, cube):
    lb = bounds['min'].values
    ub = bounds['max'].values
    cov = cube.cov().values
    tmvn = TruncatedMVN(x.values[0], cov, lb, ub)
    return pd.DataFrame(tmvn.sample(1), index=x.columns).transpose()

def normalize(tensor):
    tensor_norm = (tensor - tensor.min(axis=0).values ) / ( tensor.max(axis=0).values - tensor.min(axis=0).values)
    return tensor_norm

def standardize(tensor):
    tensor_scaled = (tensor - tensor.mean() ) / tensor.std()
    return tensor_scaled

def evaluate(parameters):
    x = torch.tensor([[parameters.get(name) for name in bounds['name']]])
    
    x = pd.DataFrame(x.detach().numpy(), columns=cube.columns)
    # return {"sum_trips": (f(x1.item()), float("nan"))}
    # x = create_mutation(x, cube)
    return f(x).iloc[0]



import os
from contextlib import contextmanager

from ax.utils.testing.mock import fast_botorch_optimize_context_manager
import plotly.io as pio

SMOKE_TEST = os.environ.get("SMOKE_TEST")
NUM_EVALS = 1200

@contextmanager
def dummy_context_manager():
    yield

if SMOKE_TEST:
    fast_smoke_test = fast_botorch_optimize_context_manager
else:
    fast_smoke_test = dummy_context_manager


import time
from wakepy import keep

with keep.running() as keep_running:
    with fast_smoke_test():
        for size in [100]:
            ax_client = create_ax_client(verbose=False)
            
            exec_time = np.empty(size+NUM_EVALS)
            
            cube, counts = create_cube_count(filename)
            cube, counts = cube.sample(size).reset_index(), counts.sample(size).reset_index()
            cube = cube.drop('index', axis=1)
            counts = counts.drop('index', axis=1)
                
            for i in range(len(cube)):
                ax_client.attach_trial(dict(zip(cube.columns, cube.iloc[i].values)))
                ax_client.complete_trial(i, counts.iloc[i].values[0])
            
            for i in tqdm(range(NUM_EVALS)):
                st = time.time()
                parameters, trial_index = ax_client.get_next_trial()
                ax_client.complete_trial(trial_index=trial_index, raw_data=evaluate(parameters))
                exec_time[trial_index-size] = time.time() - st
   
                
            data = ax_client.get_trials_data_frame()
            data['execution_time'] = np.concatenate([np.zeros(len(cube)), exec_time[:(trial_index+1-len(cube))]])
            timestamp = datetime.now().timestamp()
            data.to_csv(f'data/out/BO/ax_ei_{size}_{timestamp}.csv')
                