import pandas as pd
import numpy as np
from datetime import datetime
from matplotlib.colors import LinearSegmentedColormap
import seaborn as sns
from utils.colors import DTU_COLOR_HEX, DTU_BIN
colors = list(DTU_COLOR_HEX.values())
sns.set_palette(sns.color_palette(colors))
CM_DTU_BIN = LinearSegmentedColormap.from_list('dtu_bin', DTU_BIN, N=2)

from models.daypattern import DayPatternSim, ACTIVITIES
from models.prim import PrimClassifier, score, coverage, density
from models.sampler import Sampler, TruncatedMVN
from utils.data_loader import load_metadata, load_betas
from tqdm import tqdm
from sklearn.cluster import KMeans

ACTIVITY = 'LeisureT'
THRESHOLD = 7900
N_CLUSTERS = 64
filename = '1683818388.570091'

betas_dp = load_betas('dp')
names = list(betas_dp.keys())[:-1]

df, betas, bounds = load_metadata()


def create_cube_counts(filename):
    data = pd.read_csv(f'data/out/LHS/{filename}.csv')
    data = data.drop(['Unnamed: 0'], axis=1)
    data_standardized = (data - data.mean()) / data.std()
    cube = data[names]
    counts = data[ACTIVITY]
    return cube, counts



def create_clusters(cube, N_CLUSTERS:int = 64):
    MC_sampler = Sampler(bounds).create_sample_lhs(10000)
    km = KMeans(n_clusters=N_CLUSTERS)
    km.fit(MC_sampler)
    clusters = km.predict(cube)
    return km, clusters

def create_best(counts, clusters, N_CLUSTERS:int = 64):
    best_index = np.empty(N_CLUSTERS)
    best_values = np.empty(N_CLUSTERS)
    for k in range(N_CLUSTERS):
        mask = clusters == k
        try:
            best_index[k] = counts.iloc[mask].idxmax()
        except:
            best_index[k] = -1
        
    best_index = best_index.astype(int)
    for k in range(N_CLUSTERS):
        try:
            best_values[k] = counts.iloc[best_index[k]]
        except:
            best_values[k] = -1
    return best_index, best_values
    
DP = DayPatternSim(betas, df)


def create_mutation(x, cube):
    lb = bounds['min'].values
    ub = bounds['max'].values
    cov = cube.cov().values
    tmvn = TruncatedMVN(x.values[0], cov, lb, ub)
    return pd.DataFrame(tmvn.sample(1), index=x.columns).transpose()

def map_elite(best_index, best_values, parents, c, y_new, ancestor):
    if y_new[ACTIVITY].values > best_values[c]:
        best_index[c] = y_new.index
        best_values[c] = y_new[ACTIVITY]
        parents[c[0]].append(ancestor[-1].tolist())
    return best_index, best_values, parents

    
def one_step(cube, counts, km, best_index, best_values, parents):
    idx = np.random.choice(np.arange(N_CLUSTERS))
    x = pd.DataFrame(cube.iloc[best_index[idx]]).transpose()
    x_new = create_mutation(x, cube)
    c = km.predict(x_new)
    y_new = DP.run(x_new)
    y_new.index = [len(cube)]
    cube = pd.concat([cube, y_new[cube.columns]])
    counts = pd.concat([counts, y_new[ACTIVITY]],axis=0)
    best_index, best_values, parents = map_elite(best_index, best_values, parents,  c, y_new, x.index)
    qd_score = best_values.sum()
    return cube, counts, best_index, best_values, parents, qd_score, c

def create_qd_metrics(cube, N, clusters):
    QD_clusters = np.empty(len(cube)+N)
    QD_clusters[:len(cube)] = clusters
    QD_clusters = QD_clusters.astype(int)
    QD_score = np.empty(N)
    return QD_clusters, QD_score

N = 300

from wakepy import keep

with keep.running() as keep_running:
    for N_CLUSTERS in [32,64,128]:
        for size in [100]:
            cube, counts = create_cube_counts(filename)
            cube, counts = cube.sample(size).reset_index(), counts.sample(size).reset_index()
            cube = cube.drop('index', axis=1)
            counts = counts.drop('index', axis=1)
            km, clusters = create_clusters(cube, N_CLUSTERS)
            parents = {k: [] for k in range(N_CLUSTERS)}
            best_index, best_values = create_best(counts, clusters, N_CLUSTERS)
            QD_clusters, QD_score = create_qd_metrics(cube, N, clusters)
            for k in tqdm(range(N)):
                cube, counts, best_index, best_values, parents, _ , _ = one_step(cube, counts, km, best_index, best_values, parents)
            df = pd.concat([cube, counts], axis=1)
            timestamp = datetime.now().timestamp()
            df.to_csv(f'data/out/QD/qd_{size}_{N_CLUSTERS}_{timestamp}.csv')
            print(f"Run saved under qd_{size}_{N_CLUSTERS}_{timestamp}.csv")